cd "docker"

docker-compose stop
docker-compose up --build -d

docker-compose exec recruit-api chown -R www-data /var/www/api
docker-compose exec recruit-api composer install
docker-compose exec recruit-api php artisan key:generate
docker-compose exec recruit-api php artisan config:cache
docker-compose exec recruit-api php artisan migrate --seed

cd "../web"

npm install
npm run css-compile
npm run build
