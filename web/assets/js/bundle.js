/**
 * @module jQuery
 */
import $ from 'jquery';
window.jQuery = $; window.$ = $;

window.BootstrapAutocomplete = require('bootstrap-4-autocomplete');
