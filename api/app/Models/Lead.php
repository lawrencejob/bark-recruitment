<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Lead
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $slug
 */
class Lead extends Model
{
    protected $table = 'leads';

    protected $fillable = [
        'name',
        'email',
        'phone',
        'more_info',
        'locations_id',
        'services_id'
    ];

}