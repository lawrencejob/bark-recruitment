<?php namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Models\Lead;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Validator;


class LeadsController extends Controller
{

    /**
     * Find the nearest city based on the provided lat / lng
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:200',
            'email' => 'required|max:200',
            'phone' => 'required|max:100',
            'extra' => 'max:1000',
            'locations_id' => 'required|integer',
            'services_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()
            ]);

        }

        $input = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'extra' => $request->get('extra'),
            'locations_id' => $request->get('locations_id'),
            'services_id' => $request->get('services_id'),
        ];

        Lead::create($input);

        return response()->json([
            'status'    =>  true
        ]);
    }
}
