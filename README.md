# Bark Recruitment Challenge

Welcome to the Bark Recruitment Challenge. The aim of this readme is to get your development environment set up as 
quickly as possible, so you can spend more time coding and less messing around with docker.

# Development Workflow
You should be able to run the Bark Recruitment Challenge locally using docker. To simplify this, we have created a 
script (`start_bark.sh`).

This will:
1. Build the docker containers
2. Update required permissions
3. Run composer install
4. Perform any migrations/seeds 

If you have issues with your docker containers, and want to completely
rebuild them, you can run `force_refresh_bark.sh` which will stop and destroy
the containers before trying to rebuild them.
