terraform {
  backend "s3" {
    bucket = "bark-recruitment"
    key    = "states/terraform.tfstate"
    region = "eu-west-2"
    profile = "terraformer"
  }
}

provider "aws" {
  profile = "terraformer"
  region  = "eu-west-2"
}

locals {
  env_name = "recruit-test-${terraform.workspace}"
  domain_prefix = terraform.workspace
  domain_names = {
    web = "${local.domain_prefix}.${var.domain}"
    api = "${local.domain_prefix}-api.${var.domain}"
  }
}

data "tls_public_key" "key_file" {
  private_key_pem = file("${path.module}/keys/${terraform.workspace}")
}

module "vpc" {
  source = "./resources/vpc"
  env_name = local.env_name
}

module "route53" {
  source = "./resources/route-53"
  env_name = local.env_name
  domain = var.domain
  domain_names = local.domain_names
  instance_public_ip_address = module.ec2.public_ip_address
}

module "sg" {
  source = "./resources/security-group"

  vpc = module.vpc.vpc
  office_cidr_blocks = var.office_cidr_blocks
  env_name = local.env_name
}

module "ec2" {
  source = "./resources/ec2"

  security_groups = [module.sg.security_groups.internal]
  vpc = module.vpc.vpc
  env_name = local.env_name
  domain_prefix = local.domain_prefix
  git_repo_access_key = data.tls_public_key.key_file
}

