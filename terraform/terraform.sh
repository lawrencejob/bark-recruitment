#!/bin/bash

TF_WORKSPACE=$(terraform workspace show)

./get_keys.sh

terraform $1 -var-file "config.tfvars"

