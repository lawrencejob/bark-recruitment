# use the most recent AWS Ubuntu image

variable "security_groups" {}
variable "vpc" {}
variable "env_name" {}
variable "domain_prefix" {}


data "aws_ami" "server_ami" {

  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "ec2_server" {

  ami           = data.aws_ami.server_ami.id
  instance_type = "t2.micro"

  key_name               = "recruitment-automation-sub-ldn"
  monitoring             = false
  vpc_security_group_ids = var.security_groups
  subnet_id              = var.vpc.public_subnets.0

  associate_public_ip_address = true

  root_block_device {
    volume_size = 30
  }

  tags = {
    Name           = var.env_name
    Terraform      = "true"
    Environment    = "recruitment-tests"
    SubEnvironment = var.env_name
  }

  user_data = <<SH
#!/bin/bash

# add swap
sudo fallocate -l 4G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo echo "\n/swapfile swap swap defaults 0 0" >> /etc/fstab

# set the hostname
sudo hostname ${var.domain_prefix}

# add public key to authorized keys
sudo echo "
${var.git_repo_access_key.public_key_openssh}

" >> /home/ubuntu/.ssh/authorized_keys

# "whitelist" bitbucket
ssh-keyscan bitbucket.org >> /home/ubuntu/.ssh/known_hosts
chown /home/ubuntu/.ssh/known_hosts ubuntu

# install docker
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

curl -sSL https://get.docker.com/ | sh

sudo usermod -aG docker ubuntu

# install npm
sudo apt install -y npm

# start docker
sudo service docker start

# wait 5 seconds before saying this instance is ready
sleep 5

# mark the instance as ready
touch /home/ubuntu/instance_ready

SH

}

output "public_ip_address" {
  value = aws_instance.ec2_server.public_ip
}


resource "null_resource" "startup_script" {

  triggers = {
    cluster_instance_ids = aws_instance.ec2_server.id
    startup_script = md5(data.template_file.startup_script.rendered)
    startup_script = filemd5("${path.module}/scripts/init-script.sh")
  }

  connection {
    type        = "ssh"
    host        = aws_instance.ec2_server.public_ip
    user        = "ubuntu"
    timeout     = "30s"
    private_key = file("~/.ssh/recruitment-automation-sub-ldn.pem")
  }

  provisioner "file" {
    content     = data.template_file.startup_script.rendered
    destination = "/tmp/startup-script.sh"
  }

  provisioner "file" {
    content     = file("${path.module}/scripts/init-script.sh")
    destination = "/tmp/init-script.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/init-script.sh",
      "chmod +x /tmp/startup-script.sh",
      "/tmp/startup-script.sh",
    ]
  }
}

