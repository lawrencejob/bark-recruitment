data "aws_region" "current" {}

variable "env_name" {}

module "main_vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.env_name}-vpc"

  azs             = ["${data.aws_region.current.name}a", "${data.aws_region.current.name}b"]
  private_subnets = []
  public_subnets  = ["10.5.10.0/24", "10.5.20.0/24"]

  cidr            = "10.5.0.0/16"

  enable_nat_gateway = false
  enable_vpn_gateway = false

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Terraform = "true"
    Environment = "recruitment-tests"
  }
}

output "vpc" {
  value = module.main_vpc
}

