variable "domain" {}
variable "domain_names" {}
variable "env_name" {}
variable "instance_public_ip_address" {}

data "aws_route53_zone" "rec_zone" {
  name         = var.domain
  private_zone = false
}

resource "aws_route53_record" "web_route53_record" {

  zone_id = data.aws_route53_zone.rec_zone.zone_id
  name    = var.domain_names.web
  type    = "A"
  ttl     = "300"
  records = [var.instance_public_ip_address]
}

resource "aws_route53_record" "api_route53_record" {

  zone_id = data.aws_route53_zone.rec_zone.zone_id
  name    = var.domain_names.api
  type    = "A"
  ttl     = "300"
  records = [var.instance_public_ip_address]
}

output "routes" {
  value = {
    web = aws_route53_record.web_route53_record.*.name
    api = aws_route53_record.api_route53_record.*.name
  }
}

# resource "aws_route53_record" "alias_route53_record_internal" {
#   count = local.internal_alb_count
#
#   zone_id = data.aws_route53_zone.selected.zone_id
#   name    = "${terraform.workspace}-${count.index + 1}-internal${var.service_domain}.${var.domain}"
#   type    = "A"
#
#   alias {
#     name                   = aws_lb.internal_alb[count.index].dns_name
#     zone_id                = aws_lb.internal_alb[count.index].zone_id
#     evaluate_target_health = true
#   }
#
# }