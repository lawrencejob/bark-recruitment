variable "domain" {
  type    = string
  default = "r.bark.com"
}

variable "office_cidr_blocks" {
  type    = list(string)
  default = []
}
